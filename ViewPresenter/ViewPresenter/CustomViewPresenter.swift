//
//  CustomViewPresenter.swift
//  PeaceMeditation
//
//  Created by Beatman on 2018/8/21.
//  Copyright © 2018年 DAILYINNOVATION CO., LIMITED. All rights reserved.
//

import Foundation
import UIKit

enum PresenterAnimateType: Int {
    /** 从下向上 */
    case fromBottomToTop = 0
    /** 从下向上 */
    case fromTopToBottom = 1
    /** 直接出现 */
    case normal = 2
    /** 弹性从下向上 */
    case bouncePresent = 3
    /** 阻尼从下向上 */
    case decayPresent = 4
}

enum ViewPresenterBackgroundStyle: Int {
    /** 浅色模糊 */
    case lightBlur = 0
    /** 深色模糊 */
    case darkBlur = 1
    /** 灰色带透明度 */
    case grayTransparent = 2
    /** 透明 */
    case clear = 3
}

class ViewPresenter:NSObject, UIViewControllerTransitioningDelegate {

    weak private var viewController: UIViewController? = nil
    private var closeClosure: (()->Void)?

    private var animator: CustomPresentationAnimator

    /** animateType: 动画类型, backgroundStyle: 背景样式, presentedFrame: 动画后展示时的frame, closeClosure: dismiss后调用的block */
    init(toViewController: UIViewController,
         animateType: PresenterAnimateType,
         backgroundStyle: ViewPresenterBackgroundStyle,
         presentedFrame: CGRect,
         closeClosure: (()->Void)?) {

        self.viewController = toViewController
        self.closeClosure = closeClosure
        animator = CustomPresentationAnimator(bgStyle: backgroundStyle, animateType: animateType, presentedFrame: presentedFrame, closeClosure: closeClosure)
        super.init()
    }

    /** present: completedClosure是展示完成之后调用的block */
    func present(completedClosure: @escaping ()->Void) {
        guard let dialogVC = self.viewController else { return }
        dialogVC.modalPresentationStyle = .custom
        dialogVC.transitioningDelegate = animator
        guard let topVC = UIApplication.topViewController() else {
            return
        }
        topVC.present(dialogVC, animated: true) {
            print("ddd")
            completedClosure()
        }
    }
}

class CustomPresentationAnimator: NSObject, UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning {
    var presentedFrame = UIScreen.main.bounds
    var isPresented = false
    var closeClosure: (()->Void)? = {}
    var animateType: PresenterAnimateType = .fromBottomToTop
    var bgStyle: ViewPresenterBackgroundStyle = .clear

    init(bgStyle: ViewPresenterBackgroundStyle, animateType: PresenterAnimateType, presentedFrame: CGRect, closeClosure: (()->Void)?) {
        self.bgStyle = bgStyle
        self.animateType = animateType
        self.closeClosure = closeClosure
        self.presentedFrame = presentedFrame
    }

    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        let dialog = DialogPresentationController(presentedViewController: presented, presenting: presenting, bgStyle: bgStyle)
        dialog.presentedFrame = presentedFrame
        return dialog
    }

    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        isPresented = false
        print("show")
        return self
    }

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        isPresented = true
        closeClosure?()
        print("disappear")
        return self
    }

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        if isPresented {
            disappear(transitionContext: transitionContext)
        } else {
            showUp(transitionContext: transitionContext)
        }
    }

    func showUp(transitionContext: UIViewControllerContextTransitioning){
        guard let view = transitionContext.view(forKey: .to) else {
            return
        }
        switch self.animateType {

        case .decayPresent:
            guard let fromVC = transitionContext.viewController(forKey: .from), let toVC = transitionContext.viewController(forKey: .to) else { return }
            let containerView = transitionContext.containerView
            containerView.addSubview(toVC.view)
            var fromFrame = fromVC.view.frame
            var toFrame = self.presentedFrame
            fromFrame.origin.y = 0
            toFrame.origin.y = containerView.frame.size.height
            toVC.view.frame = toFrame
            toFrame.origin.y = 0
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.92, initialSpringVelocity: 17, options: .curveEaseIn, animations: {
                fromVC.view.frame = fromFrame
                toVC.view.frame = toFrame
            }) { (finished) in
                transitionContext.completeTransition(true)
            }

        case .bouncePresent:
            guard let fromVC = transitionContext.viewController(forKey: .from), let toVC = transitionContext.viewController(forKey: .to) else { return }
            let finalFrame = self.presentedFrame
            let containerView = transitionContext.containerView
            let screenBounds = UIScreen.main.bounds
            toVC.view.frame = finalFrame.offsetBy(dx: 0, dy: screenBounds.size.height)
            containerView.addSubview(toVC.view)
            UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: .curveLinear, animations: {
                fromVC.view.alpha = 1
                toVC.view.frame = finalFrame
            }) { (finished) in
                fromVC.view.alpha = 1
                transitionContext.completeTransition(true)
            }
        case .fromBottomToTop:
            var originFrameY = self.presentedFrame.origin.y
            originFrameY = UIScreen.main.bounds.height
            let startFrame = CGRect(x: self.presentedFrame.origin.x, y: originFrameY, width: presentedFrame.width, height: presentedFrame.height)
            view.frame = startFrame
            view.alpha = 0
            transitionContext.containerView.addSubview(view)
            UIView.animate(withDuration: 0.3, animations: {
                view.alpha = 1
                view.frame = self.presentedFrame
            }) { (completed) in
                transitionContext.completeTransition(completed)
            }

        case .fromTopToBottom:
            var originFrameY = self.presentedFrame.origin.y
            originFrameY = -UIScreen.main.bounds.height
            let startFrame = CGRect(x: self.presentedFrame.origin.x, y: originFrameY, width: presentedFrame.width, height: presentedFrame.height)
            view.frame = startFrame
            view.alpha = 0
            transitionContext.containerView.addSubview(view)
            UIView.animate(withDuration: 0.3, animations: {
                view.alpha = 1
                view.frame = self.presentedFrame
            }) { (completed) in
                transitionContext.completeTransition(completed)
            }

        default:
            view.frame = self.presentedFrame
            view.alpha = 0
            transitionContext.containerView.addSubview(view)
            UIView.animate(withDuration: 0.3, animations: {
                view.alpha = 1
            }) { (completed) in
                transitionContext.completeTransition(completed)
            }
        }
    }

    func disappear(transitionContext: UIViewControllerContextTransitioning){
        guard let view = transitionContext.view(forKey: .from) else {
            return
        }

        switch self.animateType {

        case .fromBottomToTop:
            var originFrameY = self.presentedFrame.origin.y
            originFrameY = UIScreen.main.bounds.height
            let startFrame = CGRect(x: self.presentedFrame.origin.x, y: originFrameY, width: presentedFrame.width, height: presentedFrame.height)
            UIView.animate(withDuration: 0.3, animations: {
                view.alpha = 0
                view.frame = startFrame
            }) { (completed) in
                transitionContext.completeTransition(completed)
            }

        case .fromTopToBottom:
            var originFrameY = self.presentedFrame.origin.y
            originFrameY = -UIScreen.main.bounds.height
            let startFrame = CGRect(x: self.presentedFrame.origin.x, y: originFrameY, width: presentedFrame.width, height: presentedFrame.height)
            transitionContext.containerView.addSubview(view)
            UIView.animate(withDuration: 0.3, animations: {
                view.alpha = 0
                view.frame = startFrame
            }) { (completed) in
                transitionContext.completeTransition(completed)
            }

        default:
            UIView.animate(withDuration: 0.3, animations: {
                view.alpha = 0
            }) { (completed) in
                transitionContext.completeTransition(true)
            }
        }
    }
}

class DialogPresentationController: UIPresentationController {
    var presentedFrame = CGRect.zero
    var isLayout = false
    var bgStyle: ViewPresenterBackgroundStyle = .clear
    var coverView: UIView!
    var alpha: CGFloat = 1

    init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?, bgStyle: ViewPresenterBackgroundStyle) {
        self.bgStyle = bgStyle
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
    }
    override func containerViewDidLayoutSubviews(){
        if !isLayout {
            presentedView?.frame = presentedFrame
            addCover()
            isLayout = true
        } else {
            removeCover()
        }
    }

    @objc func closeDialog(){
        presentedViewController.dismiss(animated: true, completion: nil)
    }

    private func removeCover() {
        UIView.animate(withDuration: 0.3) {
            self.coverView.alpha = 0
        }
    }

    private func addCover() {
        switch self.bgStyle {
        case .clear:
            coverView = UIView(frame: UIScreen.main.bounds)
            coverView.backgroundColor = UIColor.clear
            coverView.alpha = 0
            alpha = 1
        case .darkBlur:
            let blur = UIBlurEffect(style: .dark)
            coverView = UIVisualEffectView(effect: blur)
            coverView.backgroundColor = .clear
            coverView.frame = UIScreen.main.bounds
            coverView.alpha = 0
            alpha = 1
        case .lightBlur:
            let blur = UIBlurEffect(style: .light)
            coverView = UIVisualEffectView(effect: blur)
            coverView.backgroundColor = .clear
            coverView.frame = UIScreen.main.bounds
            coverView.alpha = 0
            alpha = 1
        default:
            coverView = UIView(frame: UIScreen.main.bounds)
            coverView.backgroundColor = UIColor.black
            coverView.alpha = 0.3
            alpha = 0.3
        }
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(closeDialog))
        coverView.addGestureRecognizer(tapGesture)
        containerView?.insertSubview(coverView, at: 0)
        UIView.animate(withDuration: 0.2) {
            self.coverView.alpha = self.alpha
        }
    }
}

extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}
